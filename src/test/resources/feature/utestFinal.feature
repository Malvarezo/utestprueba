#Autor: Miguel Alvarez
  @stories
  Feature: Registrarse en la pagina de utest
    @Scenario
    Scenario: Juan quiere registrarse en la pagina de utest
      Given El usuario desea registrarse en utest
      When El ususario diligigencia todos los datos para el registro en utest
      |strNombre|strApellido|strEmail             |strMes|strDia|strAno|strCiudad|strCodigoPostal|strPais |strComputador|strVersion|strLenguaje|strMovil|strModelo|strOs   |strContrasena|strContrasenaConfirmar |
      |Nombre   |Apellido   |CorreoEmail@gmail.com|June  |12    |1990  |Sincelejo|700001         |Colombia|Windows      |10        |Arabic     |Apple   |iPhone X |iOS 14.4|123456789/*-+|123456789/*-+          |
      Then El registro se completa cuando el usuario ve el boton de Complete Setup
      |strBotonFinal |
      |Complete Setup|
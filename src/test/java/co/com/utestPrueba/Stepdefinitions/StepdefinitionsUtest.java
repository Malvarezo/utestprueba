package co.com.utestPrueba.Stepdefinitions;

import co.com.utestPrueba.model.UtestDatos;
import co.com.utestPrueba.questions.Respuesta;
import co.com.utestPrueba.tasks.*;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.*;

public class StepdefinitionsUtest {

    @Before
    public void setOnStage(){
        setTheStage(new OnlineCast());
    }

    @Given("^El usuario desea registrarse en utest$")
    public void elUsuarioDeseaRegistrarseEnUtest(){
        theActorCalled("Juan").attemptsTo(
                AbrirPaginaUtest.enLaPagina()
        );
    }

    @When("^El ususario diligigencia todos los datos para el registro en utest$")
    public void elUsusarioDiligigenciaTodosLosDatosParaElRegistroEnUtest(List<UtestDatos> datos){
        theActorInTheSpotlight().attemptsTo(
                LlenarInformacionGeneral.enLaPagina(datos),
                LlenarDireccion.enLaPagina(datos),
                LlenarDispositivos.enLaPagina(datos),
                LlenarContrasena.enLaPagina(datos)
        );
    }

    @Then("^El registro se completa cuando el usuario ve el boton de Complete Setup$")
    public void elRegistroSeCompletaCuandoElUsuarioVeElBotonDeCompleteSetup(List<UtestDatos> datos){
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(Respuesta.es(datos)));
    }
}

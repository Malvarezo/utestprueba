package co.com.utestPrueba.Runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions (features = "src/test/resources/feature/utestFinal.feature",
        tags = "@stories",
        glue = "co.com.utestPrueba.Stepdefinitions",
        snippets = SnippetType.CAMELCASE )

public class RunnersTags {
}
package co.com.utestPrueba.questions;

import co.com.utestPrueba.model.UtestDatos;
import co.com.utestPrueba.userinterfaces.PaginaContrasena;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

public class Respuesta implements Question<Boolean> {
    private List<UtestDatos> datos;

    public Respuesta(List<UtestDatos> datos) {
        this.datos = datos;
    }

    public static Respuesta es(List<UtestDatos> datos) {
        return new Respuesta(datos);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        String texto_boton_final = Text.of(PaginaContrasena.TEXTO_BOTON_FINAL).viewedBy(actor).asString();

        return datos.get(0).getStrBotonFinal().equals(texto_boton_final);
    }
}

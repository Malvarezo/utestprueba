package co.com.utestPrueba.model;

public class UtestDatos {

    private String strNombre;
    private String strApellido;
    private String strEmail;
    private String strMes;
    private String strDia;
    private String strAno;
    private String strCiudad;
    private String strCodigoPostal;
    private String strPais;
    private String strComputador;
    private String strVersion;
    private String strLenguaje;
    private String strMovil;
    private String strModelo;
    private String strOs;
    private String strContrasena;
    private String strContrasenaConfirmar;
    private String strBotonFinal;

    public String getStrContrasena() {
        return strContrasena;
    }

    public void setStrContrasena(String strContrasena) {
        this.strContrasena = strContrasena;
    }

    public String getStrContrasenaConfirmar() {
        return strContrasenaConfirmar;
    }

    public void setStrContrasenaConfirmar(String strContrasenaConfirmar) {
        this.strContrasenaConfirmar = strContrasenaConfirmar;
    }

    public String getStrBotonFinal() {
        return strBotonFinal;
    }

    public void setStrBotonFinal(String strBotonFinal) {
        this.strBotonFinal = strBotonFinal;
    }

    public String getStrComputador() {
        return strComputador;
    }

    public void setStrComputador(String strComputador) {
        this.strComputador = strComputador;
    }

    public String getStrVersion() {
        return strVersion;
    }

    public void setStrVersion(String strVersion) {
        this.strVersion = strVersion;
    }

    public String getStrLenguaje() {
        return strLenguaje;
    }

    public void setStrLenguaje(String strLenguaje) {
        this.strLenguaje = strLenguaje;
    }

    public String getStrMovil() {
        return strMovil;
    }

    public void setStrMovil(String strMovil) {
        this.strMovil = strMovil;
    }

    public String getStrModelo() {
        return strModelo;
    }

    public void setStrModelo(String strModelo) {
        this.strModelo = strModelo;
    }

    public String getStrOs() {
        return strOs;
    }

    public void setStrOs(String strOs) {
        this.strOs = strOs;
    }

    public String getStrCiudad() {
        return strCiudad;
    }

    public void setStrCiudad(String strCiudad) {
        this.strCiudad = strCiudad;
    }

    public String getStrCodigoPostal() {
        return strCodigoPostal;
    }

    public void setStrCodigoPostal(String strCodigoPostal) {
        this.strCodigoPostal = strCodigoPostal;
    }

    public String getStrPais() {
        return strPais;
    }

    public void setStrPais(String strPais) {
        this.strPais = strPais;
    }

    public String getStrNombre() {
        return strNombre;
    }

    public void setStrNombre(String strNombre) {
        this.strNombre = strNombre;
    }

    public String getStrApellido() {
        return strApellido;
    }

    public void setStrApellido(String strApellido) {
        this.strApellido = strApellido;
    }

    public String getStrEmail() {
        return strEmail;
    }

    public void setStrEmail(String strEmail) {
        this.strEmail = strEmail;
    }

    public String getStrMes() {
        return strMes;
    }

    public void setStrMes(String strMes) {
        this.strMes = strMes;
    }

    public String getStrDia() {
        return strDia;
    }

    public void setStrDia(String strDia) {
        this.strDia = strDia;
    }

    public String getStrAno() {
        return strAno;
    }

    public void setStrAno(String strAno) {
        this.strAno = strAno;
    }
}

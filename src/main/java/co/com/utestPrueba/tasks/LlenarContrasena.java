package co.com.utestPrueba.tasks;

import co.com.utestPrueba.model.UtestDatos;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;

import static co.com.utestPrueba.userinterfaces.PaginaContrasena.*;

public class LlenarContrasena implements Task {
    private List<UtestDatos> datos;

    public LlenarContrasena(List<UtestDatos> datos) {
        this.datos = datos;
    }

    public static LlenarContrasena enLaPagina(List<UtestDatos> datos) {
        return Tasks.instrumented(LlenarContrasena.class, datos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(datos.get(0).getStrContrasena()).into(CAMPO_CONTRASENA),
                Enter.theValue(datos.get(0).getStrContrasenaConfirmar()).into(CAMPO_CONTRASENA_CONFIRMAR),
                Click.on(CHECK_1),
                Click.on(CHECK_2),
                Click.on(CHECK_3)
        );
    }
}

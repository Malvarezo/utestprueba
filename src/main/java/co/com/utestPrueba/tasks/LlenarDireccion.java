package co.com.utestPrueba.tasks;

import co.com.utestPrueba.model.UtestDatos;
import co.com.utestPrueba.userinterfaces.PaginaDireccion;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.openqa.selenium.Keys;

import java.util.List;

import static co.com.utestPrueba.userinterfaces.PaginaDireccion.*;

public class LlenarDireccion implements Task {
    private List<UtestDatos> datos;

    public LlenarDireccion(List<UtestDatos> datos) {
        this.datos = datos;
    }

    public static LlenarDireccion enLaPagina(List<UtestDatos> datos) {
        return Tasks.instrumented(LlenarDireccion.class,datos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(datos.get(0).getStrCiudad()).into(CAMPO_CIUDAD),
                Enter.theValue(datos.get(0).getStrCodigoPostal()).into(CAMPO_CODIGOPOSTAL),
                Click.on(DIV_PAIS),
                Enter.theValue(datos.get(0).getStrPais()).into(CAMPO_PAIS).thenHit(Keys.ARROW_DOWN, Keys.ENTER),
                Click.on(BOTON_NEXT)
        );

    }
}

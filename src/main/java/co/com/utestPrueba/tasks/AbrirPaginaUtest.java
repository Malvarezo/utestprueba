package co.com.utestPrueba.tasks;

import co.com.utestPrueba.userinterfaces.PaginaUtest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirPaginaUtest implements Task {
    private PaginaUtest paginaUtest;

    public static AbrirPaginaUtest enLaPagina() {
        return Tasks.instrumented(AbrirPaginaUtest.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.wasAbleTo(
                Open.browserOn(paginaUtest)
        );

    }
}

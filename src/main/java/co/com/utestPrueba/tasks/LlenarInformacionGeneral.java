package co.com.utestPrueba.tasks;

import co.com.utestPrueba.model.UtestDatos;
import co.com.utestPrueba.userinterfaces.PaginaInformacionGeneral;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import java.util.List;

import static co.com.utestPrueba.userinterfaces.PaginaInformacionGeneral.*;

public class LlenarInformacionGeneral implements Task {
    private List<UtestDatos> datos;

    public LlenarInformacionGeneral(List<UtestDatos> datos) {
        this.datos = datos;
    }

    public static LlenarInformacionGeneral enLaPagina(List<UtestDatos> datos) {
        return Tasks.instrumented(LlenarInformacionGeneral.class, datos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BOTON_JOIN),
                Enter.theValue(datos.get(0).getStrNombre()).into(CAMPO_NOMBRE),
                Enter.theValue(datos.get(0).getStrApellido()).into(CAMPO_APELLIDO),
                Enter.theValue(datos.get(0).getStrEmail()).into(CAMPO_EMAIL),
                SelectFromOptions.byVisibleText(datos.get(0).getStrMes()).from(MES),
                SelectFromOptions.byVisibleText(datos.get(0).getStrDia()).from(DIA),
                SelectFromOptions.byVisibleText(datos.get(0).getStrAno()).from(ANO),
                Click.on(BOTON_NEXT)
        );

    }
}

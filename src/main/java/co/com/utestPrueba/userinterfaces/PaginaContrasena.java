package co.com.utestPrueba.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PaginaContrasena {
    public static final Target CAMPO_CONTRASENA = Target.the("Contrasena").located(By.id("password"));
    public static final Target CAMPO_CONTRASENA_CONFIRMAR = Target.the("Contrasena confirma").located(By.id("confirmPassword"));

    public static final Target CHECK_1 = Target.the("Check 1").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[4]/label/span"));
    public static final Target CHECK_2 = Target.the("Check 2").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[5]/label/span[1]"));
    public static final Target CHECK_3 = Target.the("Check 3").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[6]/label/span[1]"));

    public static final Target TEXTO_BOTON_FINAL = Target.the("Boton final").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div/div/a/span"));

}

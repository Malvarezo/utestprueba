package co.com.utestPrueba.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PaginaInformacionGeneral {

    public static final Target BOTON_JOIN = Target.the("Boton Join").located(By.xpath("/html/body/ui-view/unauthenticated-container/div/div/unauthenticated-header/div/div[3]/ul[2]/li[2]/a"));
    public static final Target CAMPO_NOMBRE = Target.the("Nombre").located(By.id("firstName"));
    public static final Target CAMPO_APELLIDO = Target.the("Apellido").located(By.id("lastName"));
    public static final Target CAMPO_EMAIL = Target.the("Email").located(By.id("email"));
    public static final Target MES = Target.the("Mes").located(By.id("birthMonth"));
    public static final Target DIA = Target.the("Dia").located(By.id("birthDay"));
    public static final Target ANO = Target.the("Ano").located(By.id("birthYear"));

    public static final Target BOTON_NEXT = Target.the("Boton next").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[2]/a"));
}
